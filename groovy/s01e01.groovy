class Todo {
    String titulo
    String descripcion
}

todos = [new Todo(titulo:"Lavadora", descripcion:"Poner lavadora"), new Todo(titulo:"Impresora", descripcion:"Comprar cartuchos impresora"), new Todo(titulo:"Películas", descripcion:"Devolver películas videoclb")]

todos.each { todo -> println todo.getTitulo() + ' ' + todo.getDescripcion() }

return