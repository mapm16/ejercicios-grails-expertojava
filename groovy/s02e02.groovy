class Calculadora {
    def num1
    def num2
    def oper
    
    Calculadora(def num1, def num2, def oper) {
        this.num1 = num1
        this.num2 = num2
        this.oper = oper
    }
    
   def cal() {
       if (oper == "+") 
          return num1 + num2 
       
       if (oper == "-") 
          return num1 - num2 
       
       if (oper == "*")
           return num1 * num2
           
       if (oper == "/")
           return num1 / num2
   }
}

System.in.withReader {
  print 'Introduzca operador 1: '
  op1 = it.readLine()
  println(op1)
  print 'Introduzca operador 2: '
  op2 = it.readLine()
  println(op2)
  print 'Introduzca la operacion: '
  op3 = it.readLine()
  println(op3)
}

def calc = new Calculadora(op1.toDouble(), op2.toDouble(), op3)
println calc.cal()