/* Añade aquí la implementación del factorial en un closure */
def factorial

factorial = { Integer n ->
    if (n == 1) return 1
    n * factorial(n - 1)
}

assert factorial(1)==1
assert factorial(2)==1*2
assert factorial(3)==1*2*3
assert factorial(4)==1*2*3*4
assert factorial(5)==1*2*3*4*5
assert factorial(6)==1*2*3*4*5*6
assert factorial(7)==1*2*3*4*5*6*7
assert factorial(8)==1*2*3*4*5*6*7*8
assert factorial(9)==1*2*3*4*5*6*7*8*9
assert factorial(10)==1*2*3*4*5*6*7*8*9*10

/* Añade a partir de aquí el resto de tareas solicitadas en el ejercicios */
def listadoFactorial = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]

listadoFactorial.each { 
    println factorial(it)
}

def ayer
def mañana

ayer = { Date fecha ->
    return fecha - 1
}

mañana = { Date fecha ->
    return fecha + 1
}

def listadoFecha = [new Date().parse("d/M/yyyy H:m:s","28/6/2008 00:30:20"), new Date().parse("d/M/yyyy H:m:s","22/7/2008 00:30:20"), new Date().parse("d/M/yyyy H:m:s","31/1/2008 00:30:20")]

listadoFecha.each { 
    println ayer(it)
    println mañana(it)
}
return