package es.ua.expertojava.todo


import grails.test.mixin.TestFor
import spock.lang.Specification

/**
 * See the API for {@link grails.test.mixin.services.ServiceUnitTestMixin} for usage instructions
 */
@TestFor(TodoService)
class TodoServiceSpec extends Specification {

    def setup() {
    }

    def cleanup() {
    }

    void "El método getNextTodos devuelve los siguientes todos de los días pasado por parámetro"() {
        given:
        def user1 = new User(username:"usuario1", password:"usuario1", name:"usuario1", surnames:"usuario1", confirmPassword:"usuario1", email:"usuario1@ua.es")
        def todoDayBeforeYesterday = new Todo(title:"Todo day before yesterday", date: new Date() - 2 , reminderDate: new Date() - 3, finalizada: false, user:user1)
        def todoYesterday = new Todo(title:"Todo yesterday", date: new Date() - 1 , reminderDate: new Date() - 2, finalizada: false,user:user1)
        def todoToday = new Todo(title:"Todo today", date: new Date(), reminderDate:  new Date() - 1, finalizada: false,user:user1)
        def todoTomorrow = new Todo(title:"Todo tomorrow", date: new Date() + 1 , reminderDate:  new Date() , finalizada: false,user:user1)
        def todoDayAfterTomorrow = new Todo(title:"Todo day after tomorrow", date: new Date() + 2 , reminderDate: new Date() - 1,  finalizada: false,user:user1)
        def todoDayAfterDayAfterTomorrow = new Todo(title:"Todo day after tomorrow", date: new Date() + 3 , reminderDate: new Date() + 2, finalizada: false,user:user1)
        and:
        mockDomain(Todo,[todoDayBeforeYesterday, todoYesterday, todoToday, todoTomorrow, todoDayAfterTomorrow, todoDayAfterDayAfterTomorrow])
        and:
        def nextTodos = service.getNextTodos(2,[:])
        expect:
        Todo.count() == 6
        and:
        nextTodos.containsAll([todoTomorrow, todoDayAfterTomorrow])
        nextTodos.size() == 2
        and:
        !nextTodos.contains(todoDayBeforeYesterday)
        !nextTodos.contains(todoToday)
        !nextTodos.contains(todoYesterday)
        !nextTodos.contains(todoDayAfterDayAfterTomorrow)
    }

    void "Correcto almacenamiento de todo"() {
        given:
            def user1 = new User(username:"usuario1", password:"usuario1", name:"usuario1", surnames:"usuario1", confirmPassword:"usuario1", email:"usuario1@ua.es")
            def tarea1 = new Todo(title:"Pintar cocina", date:new Date() + 1, reminderDate:new Date(), finalizada:false, user: user1)
            def tarea2 = new Todo(title:"Recoger correo postal", date:new Date() + 2, reminderDate:new Date() + 1, finalizada:false, user: user1)
        and:
           mockDomain(Todo,[tarea1, tarea2])
        and:
            service.saveTodo(tarea1)
            service.saveTodo(tarea2)

        expect:
            Todo.count() == 2
    }

}
