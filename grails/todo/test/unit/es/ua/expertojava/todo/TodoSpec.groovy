package es.ua.expertojava.todo

import grails.test.mixin.TestFor
import spock.lang.Specification
import spock.lang.Unroll

/**
 * See the API for {@link grails.test.mixin.domain.DomainClassUnitTestMixin} for usage instructions
 */
@TestFor(Todo)
class TodoSpec extends Specification {

    def setup() {
    }

    def cleanup() {
    }

    @Unroll
    def "Si la fecha de recordatorio es anterior a la fecha de la propia tarea funciona"() {
        given:
            def tarea = new Todo(title:"tarea", date:new Date(), reminderDate:new Date() - numDias, finalizada:false)
        when:
            tarea.validate()
        then:
            !tarea?.errors['reminderDate']
        where:
            numDias << [1,2,3]
    }

    @Unroll
    def "Si la fecha de recordatorio es posterior a la fecha de la propia tarea falla"() {
        given:
            def tarea = new Todo(title:"tarea", date:new Date(), reminderDate:new Date() + numDias, finalizada:false)
        when:
            tarea.validate()
        then:
            tarea?.errors['reminderDate']
        where:
            numDias << [1,2,3]
    }
}
