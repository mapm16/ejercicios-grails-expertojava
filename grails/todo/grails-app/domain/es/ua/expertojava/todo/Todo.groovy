package es.ua.expertojava.todo

class Todo {
    String title
    String description
    Date date
    Date reminderDate
    String url
    Category category
    Boolean finalizada
    Date dateCreated
    Date lastUpdated
    Date dateDone
    User user

    static hasMany = [tags:Tag]
    static belongsTo = [Tag]

    static searchable = [except: ['date', 'reminderDate']]

    static constraints = {
        title(blank:false)
        description(blank:true, nullable:true, maxSize:1000)
        date(nullable:false)
        reminderDate(nullable:false, validator: {
            val, obj ->
                if (val && obj?.date) {
                    return val.before(obj?.date)
                }
                return true
        })
        url(nullable:true, url:true)
        category(nullable:true)
        finalizada(nullable:false)
        dateDone(nullable:true)
        user(nullable:true)
    }

    String toString(){
        title
    }
}
