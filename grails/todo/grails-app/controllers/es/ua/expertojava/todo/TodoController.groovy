package es.ua.expertojava.todo

import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class TodoController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]
    def todoService
    def springSecurityService

    def index(Integer max) {
        def user = springSecurityService.currentUser

        params.max = Math.min(max ?: 10, 100)
        respond Todo.findAllByUser(user, params), model:[todoInstanceCount: Todo.count()]
        //todoService.lastTodosDone(1)
    }

    def show(Todo todoInstance) {
        respond todoInstance
    }

    def create() {
        respond new Todo(params)
    }

    @Transactional
    def save(Todo todoInstance) {
        if (todoInstance == null) {
            notFound()
            return
        }

        if (todoInstance.hasErrors()) {
            respond todoInstance.errors, view:'create'
            return
        }
        def user = springSecurityService.currentUser
        todoInstance.user = user

        //todoInstance.save flush:true
        todoService.saveTodo(todoInstance)

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'todo.label', default: 'Todo'), todoInstance.id])
                redirect todoInstance
            }
            '*' { respond todoInstance, [status: CREATED] }
        }
    }

    def edit(Todo todoInstance) {
        respond todoInstance
    }

    @Transactional
    def update(Todo todoInstance) {
        if (todoInstance == null) {
            notFound()
            return
        }

        if (todoInstance.hasErrors()) {
            respond todoInstance.errors, view:'edit'
            return
        }

        //todoInstance.save flush:true
        todoService.saveTodo(todoInstance)

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'Todo.label', default: 'Todo'), todoInstance.id])
                redirect todoInstance
            }
            '*'{ respond todoInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(Todo todoInstance) {
        if (todoInstance == null) {
            notFound()
            return
        }

        //todoInstance.delete flush:true
        todoService.deleteTodo(todoInstance)

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'Todo.label', default: 'Todo'), todoInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'todo.label', default: 'Todo'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }

    def listCategory() {
        respond Category.list()
    }

    def listByCategory(params) {
        def categorias = params.seleccionadas.collect{it.toLong()}

        respond Todo.findAllByCategoryInList(Category.getAll(categorias))
    }

    def listNextTodos(Integer days) {
        respond todoService.getNextTodos(days, params),
                model:[todoInstanceCount: todoService.countNextTodos(days)],
                view:"index"
    }

    def putHour(Todo todoInstance) {
        respond todoInstance
    }

    def showTodosByUser(){
        def user = User.findByUsername(params.username)
        respond Todo.findAllByUser(user)
    }

    def indexSearch() {
        render(view:'index', model: [todoInstanceList: Todo.search([result: 'every']) {
            must(queryString(params.look))
            must(term('$/Todo/user/id', User.findByUsername(springSecurityService.currentUser.username)?.id))
        }])
    }
}
