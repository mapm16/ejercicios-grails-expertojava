package es.ua.expertojava.todo

class TodoTagLib {
    static defaultEncodeAs = [taglib:'none']
    //static encodeAsForTags = [tagName: [taglib:'html'], otherTagName: [taglib:'none']]

    static namespace = 'todo'

    def printIconFromBoolean = { att ->
      if (att.value)
        out << asset.image(src:"ok.ico", width:"20px", height:"20px")
      else
        out << asset.image(src:"ko.png", width:"20px", height:"20px")
    }
}
