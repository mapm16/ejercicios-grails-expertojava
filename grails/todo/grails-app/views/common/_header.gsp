<div id="menu">
    <nobr>
        <sec:ifLoggedIn>
            <b>Welcome Back <sec:username/>!</b>
            <!--<g:link controller="logout" action="index">Logout</g:link>-->
            <form name="logout" method="POST" action="${createLink(controller:'logout') }">
                <input type="submit" value="logout">
            </form>
        </sec:ifLoggedIn>
        <sec:ifNotLoggedIn>
            <g:link controller="login" action="auth">Login</g:link>
        </sec:ifNotLoggedIn>
    </nobr>
</div>