
<%@ page import="es.ua.expertojava.todo.Category" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <g:set var="entityName" value="${message(code: 'category.label', default: 'Category')}" />
    <title><g:message code="default.list.label" args="[entityName]" /></title>
</head>
<body>
<a href="#list-category" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
<div class="nav" role="navigation">
    <ul>
        <li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
    </ul>
</div>
<div id="list-category" class="content scaffold-list" role="main">
    <h1><g:message code="default.list.label" args="[entityName]" /></h1>
    <g:if test="${flash.message}">
        <div class="message" role="status">${flash.message}</div>
    </g:if>
    <table>
        <thead>
        <tr>
            <g:sortableColumn property="name" title="Caterorías" />
        </tr>
        </thead>
        <tbody>
        <g:form url="[resource:todoInstance, action:'listByCategory']">
            <g:each in="${categoryInstanceList}" status="i" var="categoryInstance">
                <tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
                    <td>
                        <g:checkBox name="seleccionadas" value="${fieldValue(bean: categoryInstance, field: "id")}" checked="false"/>
                        ${fieldValue(bean: categoryInstance, field: "name")}
                    </td>
                </tr>
            </g:each>
            <fieldset class="buttons">
                <g:actionSubmit class="listByCategory" action="listByCategory" value="Informe" />
            </fieldset>
        </g:form>
        </tbody>
    </table>
    <div class="pagination">
        <g:paginate total="${categoryInstanceCount ?: 0}" />
    </div>
</div>
</body>
</html>
