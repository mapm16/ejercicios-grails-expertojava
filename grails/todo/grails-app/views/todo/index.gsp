
<%@ page import="es.ua.expertojava.todo.Todo" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'todo.label', default: 'Todo')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#list-todo" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
				<li><g:link class="listCategory" action="listCategory"><label>Nuevo Informe</label></g:link></li>
			</ul>
		</div>

		<div>
			<form action="indexSearch" method="get" id="searchableForm" name="searchableForm" >
				<input type="text" name="look" value="" size="50" id="look" />
				<input type="submit" value="Search" />
			</form>
		</div>
		<div id="list-todo" class="content scaffold-list" role="main">
			<h1><g:message code="default.list.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
				<div class="message" role="status">${flash.message}</div>
			</g:if>
			<table>
			<thead>
					<tr>
						<g:sortableColumn property="options" title="Opciones" />

						<g:sortableColumn property="title" title="${message(code: 'todo.title.label', default: 'Title')}" />

						<!--<g:sortableColumn property="description" title="${message(code: 'todo.description.label', default: 'Description')}" />-->

						<g:sortableColumn property="date" title="${message(code: 'todo.date.label', default: 'Date')}" />

						<g:sortableColumn property="dateCreated" title="${message(code: 'todo.dateCreated.label', default: 'Date Created')}" />

						<g:sortableColumn property="lastUpdated" title="${message(code: 'todo.lastUpdated.label', default: 'Last Updated')}" />

						<!--<g:sortableColumn property="reminderDate" title="${message(code: 'todo.reminderDate.label', default: 'Reminder Date')}" />-->

						<g:sortableColumn property="url" title="${message(code: 'todo.url.label', default: 'Url')}" />

						<g:sortableColumn property="done" title="Done" />

						<g:sortableColumn property="dateDone" title="${message(code: 'todo.dateDone.label', default: 'Date Done')}" />

						<th><g:message code="todo.category.label" default="Category" /></th>

					</tr>
				</thead>
				<tbody>
				<g:each in="${todoInstanceList}" status="i" var="todoInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
						<td>
						<g:form url="[resource:todoInstance, action:'delete']" method="DELETE">
							<fieldset class="buttons">
								<g:link class="edit" action="edit" resource="${todoInstance}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
								<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
							</fieldset>
						</g:form>
						</td>

						<td><g:link action="show" id="${todoInstance.id}">${fieldValue(bean: todoInstance, field: "title")}</g:link></td>

						<!--<td>${fieldValue(bean: todoInstance, field: "description")}</td>-->

						<td><g:formatDate date="${todoInstance.date}" /></td>

						<td><g:formatDate date="${todoInstance.dateCreated}" /></td>

						<td><g:formatDate date="${todoInstance.lastUpdated}" /></td>

						<!--<td><g:formatDate date="${todoInstance.reminderDate}" /></td>-->

						<td>${fieldValue(bean: todoInstance, field: "url")}</td>

						<td><todo:printIconFromBoolean value="${todoInstance.finalizada}"/></td>

						<td><g:formatDate date="${todoInstance.dateDone}" /></td>

						<td>${fieldValue(bean: todoInstance, field: "category")}</td>

					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<g:paginate total="${todoInstanceCount ?: 0}" />
			</div>
		</div>
	</body>
</html>
