package es.ua.expertojava.todo

import grails.transaction.Transactional

//@Transactional
class CategoryService {

    def deleteCategory(Category categoryInstance) {
        categoryInstance.todos.each {
            it.category = null
        }
        categoryInstance.delete()
    }
}
