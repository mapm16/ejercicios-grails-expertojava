package es.ua.expertojava.todo

import grails.transaction.Transactional
import groovy.time.TimeCategory

//@Transactional
class TodoService {

    def deleteTodo(Todo todoInstance) {
        todoInstance.tags.each {
            it.todos.remove(todoInstance)
        }

        todoInstance.delete()
    }

    def saveTodo(Todo todoInstance) {
        if (todoInstance.finalizada == true)
            todoInstance.dateDone = new Date()

        todoInstance.save()
    }

    def getNextTodos(Integer days, params) {
        Date now = new Date(System.currentTimeMillis())
        Date to = now + days

        Todo.findAllByDateBetween(now, to, params)
    }

    def countNextTodos(Integer days) {
        Date now = new Date(System.currentTimeMillis())
        Date to = now + days

        Todo.countByDateBetween(now, to)
    }

    def lastTodosDone(Integer hours) {
        def before

        use( TimeCategory ) {
            before = new Date() - hours.hour
        }

        def now = new Date()

        Todo.findByDateBetween(now, before)
    }
}
