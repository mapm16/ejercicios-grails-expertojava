import es.ua.expertojava.todo.*

class BootStrap {

    def init = { servletContext ->
        def categoryHome = new Category(name:"Hogar").save()
        def categoryJob = new Category(name:"Trabajo").save()

        def tagEasy = new Tag(name:"Fácil", color:"#FF0000").save()
        def tagDifficult = new Tag(name:"Difícil", color:"#000000").save()
        def tagArt = new Tag(name:"Arte", color:"#FFFFFF").save()
        def tagRoutine = new Tag(name:"Rutina", color:"#00FF00").save()
        def tagKitchen = new Tag(name:"Cocina", color:"#FF00FF").save()


        def todoPaintKitchen = new Todo(title:"Pintar cocina", date:new Date() + 1, reminderDate:new Date(), finalizada:false)
        def todoCollectPost = new Todo(title:"Recoger correo postal", date:new Date() + 2, reminderDate:new Date() + 1, finalizada:false)
        def todoBakeCake = new Todo(title:"Cocinar pastel", date:new Date() + 4, reminderDate:new Date() + 2, finalizada:false)
        def todoWriteUnitTests = new Todo(title:"Escribir tests unitarios", date:new Date(), reminderDate:new Date() - 1, finalizada:false)

        def user1 = new User(username:"admin", password:"admin", name:"admin", surnames:"admin", confirmPassword:"admin", email:"admin@ua.es").save()
        def user2 = new User(username:"usuario1", password:"usuario1", name:"usuario1", surnames:"usuario1", confirmPassword:"usuario1", email:"usuario1@ua.es").save()
        def user3 = new User(username:"usuario2", password:"usuario2", name:"usuario2", surnames:"usuario2", confirmPassword:"usuario2", email:"usuario2@ua.es").save()
        def role1 = new Role(authority:"ROLE_ADMIN").save()
        def role2 = new Role(authority:"ROLE_BASIC").save()

        PersonRole.create user1, role1, true
        PersonRole.create user2, role2, true
        PersonRole.create user3, role2, true

        todoPaintKitchen.addToTags(tagDifficult)
        todoPaintKitchen.addToTags(tagArt)
        todoPaintKitchen.addToTags(tagKitchen)
        todoPaintKitchen.category = categoryHome
        todoPaintKitchen.save()

        todoCollectPost.addToTags(tagRoutine)
        todoCollectPost.category = categoryJob
        todoCollectPost.save()

        todoBakeCake.addToTags(tagEasy)
        todoBakeCake.addToTags(tagKitchen)
        todoBakeCake.category = categoryHome
        todoBakeCake.save()

        todoWriteUnitTests.addToTags(tagEasy)
        todoWriteUnitTests.category = categoryJob
        todoWriteUnitTests.save()

        todoPaintKitchen.user = user2
        todoPaintKitchen.save()

        todoCollectPost.user = user3
        todoCollectPost.save()
    }
    def destroy = { }
}